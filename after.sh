#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.

#echo "========================="
#echo "#Setting up mongodb:"
#echo "========================="
#apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
#echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
#sudo apt-get update
#sudo apt-get install -y mongodb-org
##sudo touch /etc/systemd/system/mongodb.service
##FILE="/etc/systemd/system/mongodb.service"
#echo "========================="
#echo "Create mongodb service file:"
#
#sudo cat <<EOT >> /etc/systemd/system/mongodb.service
#[Unit]
#Description=High-performance, schema-free document-oriented database
#After=network.target
#
#[Service]
#User=mongodb
#ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf
#
#[Install]
#WantedBy=multi-user.target
#EOT
#echo "========================="
#echo "start mongodb service:"
#sudo systemctl start mongodb
#echo "========================="
#echo "check status of mongodb service:"
#sudo systemctl status mongodb
#echo "========================="
#echo "Enable automatically starting MongoDB when the system starts:"
#sudo systemctl enable mongodb
#
#echo "========================="
#echo "install php extension for mongo via pecl:"
#sudo pecl install mongodb

sudo apt-get install mongodb
echo "========================="
sudo apt-get install pkg-config
echo "========================="
echo "install php extension for mongo via pecl:"
sudo pecl install mongodb-1.1.9
echo "========================="
echo "setup new mongo db:"
mongo
use laratest