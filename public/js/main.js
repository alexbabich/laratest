$( function() {
    $( ".sortable" ).disableSelection();

    $( ".sortable" ).sortable({
        change: function( event, ui ) {
        },
        update: function( event, ui ) {
            var order = $(this).sortable('serialize');
//                    console.log(order);
            $wishlistId = $(this).data('wishlist-id');
            $.ajax({
                method: "PATCH",
                url: "/wishlists/" + $wishlistId + "/order-wishes",
                data: order
            })
                .done(function( msg ) {
                    console.log( msg );
                });
        }
    });

    $('body').on('click', '.add-wish', function (event) {
        var lastLi = $('#sortable-create li').last();
        var number = lastLi.data('number');
        number++;
        var createView = `@include("wishes.create")`;
        var createViewUpdated = createView.replace(/NUMBER/g, number);
        $('#sortable-create').append(createViewUpdated);
    });

    $('body').on('click', '.delete-new-wish', function (event) {
        $(this).closest('li').remove();
    });

    $('body').on('click', '.delete-wish', function (event) {
        var $liElement = $(this).closest('li');
        var wishId = $liElement.data('wish-id');
        var token = $liElement.data('token');
        $.ajax({
            url: "/wishes/" + wishId,
            type: 'post',
            data: {_method: 'delete', _token :token}
        })
            .success(function(msg) {
                if (msg['status'] == 'success') {
                    $liElement.remove();
                }
            });
    });

    $('body').on('click', '.delete-wishlist', function (event) {
        var $liElement = $(this).parent('li');
        var wishlistId = $liElement.data('wish-id');
        var token = $liElement.data('token');
        $.ajax({
            url: "/wishlists/" + wishlistId,
            type: 'post',
            data: {_method: 'delete', _token :token}
        })
            .success(function(msg) {
                if (msg['status'] == 'success') {
                    $liElement.remove();
                }
            });
    });

    $('body').on('click', '.check-wish', function (event) {
        var $liElement = $(this).closest('li');
        var wishId = $liElement.data('wish-id');
        var isChecked = $liElement.data('checked');
        var token = $liElement.data('token');
        var inverseChecked = 1;
        if (isChecked == 1) {
            inverseChecked = 0;
        }
        $.ajax({
            method: "post",
            url: "/wishes/" + wishId + "/check",
            data: {_method: 'patch', _token :token, 'is_checked' : inverseChecked}
        })
        .success(function(msg) {
            if (msg['status'] == 'success') {
                var textDecoration = '';
                if (msg['checked'] == 1) {
                    textDecoration = 'line-through';
                }
                $liElement.find('a#wish-' + wishId).css('text-decoration', textDecoration);
                $liElement.data('checked', msg['checked']);
            }
        });
    });

    $('body').on('click', '.wishes-list', function (event) {
        window.location.href = '/wishlists/' + $(this).parent().data('wish-id');
    });
} );
