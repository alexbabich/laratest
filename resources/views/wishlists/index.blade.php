@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="row">
            <div>
                <h1>My Wishlists</h1>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="/wishlists/create">Add new wishlist</a>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Wishlists:</div>
                <div class="panel-body">
                    <ul>
                        @foreach($wishlists as $wishlist)
                            @include('wishlists.users-wishlist')
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
