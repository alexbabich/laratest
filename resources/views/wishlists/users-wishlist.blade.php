<li class="wishlists-item" data-wish-id="{{ $wishlist->id }}" data-token="{{ csrf_token() }}">
    <span class="delete-wishlist">
        <i class="fa fa-times-circle"></i>
    </span>
    <div class="wishlist-title">{{ $wishlist->name }}
        {{--@if($wishlist->is_private == false)--}}
            {{--<span class="glyphicon glyphicon-eye-open pull-right" aria-hidden="true"></span>--}}
        {{--@else--}}
            {{--<span class="glyphicon glyphicon-eye-close pull-right" aria-hidden="true"></span>--}}
        {{--@endif--}}
    </div>
    <div class="wishes-list">
        <ul class="row">
            @foreach($wishlist->wishesOrderedByPosition() as $wish)
                <li class="wishes-item">
                    @if($wish->is_checked)<del>@endif
                        {{ $wish->name }}
                    @if($wish->is_checked)</del>@endif
                </li>
            @endforeach
        </ul>
    </div>
    @if($wishlist->is_private != false)
        <span class="visibility glyphicon glyphicon-lock pull-right" aria-hidden="true"></span>
        {{--<span class="visibility glyphicon glyphicon-eye-open pull-right" aria-hidden="true"></span>--}}
    {{--@else--}}
        {{--<span class="visibility private-wl glyphicon glyphicon-eye-close pull-right" aria-hidden="true"></span>--}}
    @endif
</li>
