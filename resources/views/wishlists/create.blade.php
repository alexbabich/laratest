@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
            <h1>Create new wishlist</h1>
        </div>
        <form method="POST" action="/wishlists">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="wishlist[name]">Name</label>
                <input type="text" class="form-control" name="wishlist[name]" id="name" placeholder="Name">
            </div>
            <div class="checkbox">
                <label for="wishlist[is_private]">
                    <input type="hidden" name="wishlist[is_private]" value="0">
                    {{ Form::checkbox('wishlist[is_private]') }} Private
                    {{--<input type="checkbox" name="is_private" id="is_private">Private--}}
                </label>
            </div>
            <div class="form-group">
                <label for="wishlist[description]">Description</label>
                <textarea class="form-control" name="wishlist[description]" id="description" placeholder="Description"></textarea>
            </div>
            <hr>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">Wishes:</div>
                        <div class="col-md-2">
                            <a class="add-wish btn btn-primary btn-sm pull-right" href="#"><span class="glyphicon glyphicon-plus"></span></a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <ul id="sortable-create">
                        @include('wishes.create', ['number' => 0])
                    </ul>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</div>
@stop
