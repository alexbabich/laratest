@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="row">
                <h1>{{ $wishlist->name }}
                    @if($wishlist->is_private != false)
                        <span class="glyphicon glyphicon-lock pull-right" aria-hidden="true"></span>
                    @endif
                </h1>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <b>
                    @if($wishlist->is_private == false)
                        Public wishlist
                    @else
                        Private wishlist
                    @endif
                    </b>
                </div>
                <div class="col-md-12">
                    @if(!Auth::guest() && $wishlist->user->id == Auth::user()->id)
                        <a href="/wishlists/{{ $wishlist->id }}/edit" class="btn btn-primary">Edit</a>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">Created:</div>
                <div class="col-md-10">{{ $wishlist->created_at }}</div>
            </div>
            <div class="row">
                <div class="col-md-2">Description:</div>
                <div class="col-md-10">{{ $wishlist->description }}</div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Wishes:</div>
                <div class="panel-body">
                    <ul class="sortable" data-wishlist-id="{{$wishlist->id}}">
                    @foreach($wishes as $wish)
                        @include('wishes.show', ['wish' => $wish])
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
