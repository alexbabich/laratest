<li class="ui-state-default" id="item-{{ $wish->id }}" data-wish-id="{{ $wish->id }}" data-token="{{ csrf_token() }}" data-number="{{ $number or "NUMBER"}}">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <a role="button" data-toggle="collapse" href="#collapseExample{{$wish->id or "NUMBER"}}" aria-expanded="false" aria-controls="collapseExample{{$wish->id or "NUMBER"}}">
        @if($wish->is_checked)<del>@endif
        <b>{{ $wish->name or 'New wish' }}</b>
        @if($wish->is_checked)</del>@endif
    </a>
    <a class="delete-wish">
        <span class="glyphicon glyphicon-trash pull-right"></span>
    </a>
    <div class="collapse" id="collapseExample{{$wish->id or "NUMBER"}}">
        <hr>
        {{--<div class="well">--}}
            <div class="form-group">
                <label for="wishes-name{{$number or "NUMBER"}}">Name:</label>
                <input type="text" class="form-control" value="{{ $wish->name or "" }}" name="wishes[{{$number or "NUMBER"}}][name]" id="wishes-name{{$number or "NUMBER"}}" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="wishes-description{{$number or "NUMBER"}}">Description:</label>
                <textarea class="form-control" name="wishes[{{$number or "NUMBER"}}][description]" id="wishes-description{{$number or "NUMBER"}}"
                          placeholder="Description">{{ $wish->description or "" }}</textarea>
            </div>
            <div class="form-group">
                <label for="wishes-price{{$number or "NUMBER"}}">Price:</label>
                <input type="number" min="0.00" step="0.01" value="{{ $wish->price or "0.00" }}" class="form-control" name="wishes[{{$number or "NUMBER"}}][price]" id="wishes-price{{$number or "NUMBER"}}" placeholder="Price">
            </div>
        @if(!isset($wish))
            <input type="hidden" name="wishes[{{$number or "NUMBER"}}][position]" value="{{$wish->position or "NUMBER"}}">
        @endif
            <input type="hidden" name="wishes[{{$number or "NUMBER"}}][is_checked]" value="{{$wish->is_checked or 0}}">
            <input type="hidden" name="wishes[{{$number or "NUMBER"}}][id]" value="{{$wish->id}}">
        {{--</div>--}}
    </div>
{{--<hr>--}}
</li>
