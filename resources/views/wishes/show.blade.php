<li class="ui-state-default" id="item-{{ $wish->id }}" data-wish-id="{{ $wish->id }}" data-checked="{{ $wish->is_checked }}" data-token="{{ csrf_token() }}">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <a id="wish-{{ $wish->id }}" role="button" data-toggle="collapse" href="#collapseExample{{ $wish->id }}" aria-expanded="false" aria-controls="collapseExample{{ $wish->id }}" style="{{ !$wish->is_checked?'':'text-decoration:line-through' }}">
        {{--@if($wish->is_checked)<del>@endif--}}
                <b>{{ $wish->name }}</b>
        {{--@if($wish->is_checked)</del>@endif--}}
    </a>
    <div class="pull-right row">
        <div class="col-sm-6">
            <a class="check-wish">
                <span class="glyphicon glyphicon-ok"></span>
            </a>
        </div>
        <div class="col-sm-6">
            <a class="delete-wish">
                <span class="glyphicon glyphicon-trash"></span>
            </a>
        </div>
    </div>
    <div class="collapse" id="collapseExample{{ $wish->id }}">
        <hr>
        <div class="row">
            <div class="col-md-2">
                Description:
            </div>
            <div class="col-md-10">
                {{ $wish->description }}
            </div>
            <div class="col-md-2">
                Price:
            </div>
            <div class="col-md-10">
                {{ $wish->price }}
            </div>
        </div>
    </div>
</li>
