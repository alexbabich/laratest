<li class="ui-state-default" id="item-{{ $number or "NUMBER" }}" data-number="{{ $number or "NUMBER"}}">
    <a role="button" data-toggle="collapse" href="#collapseExample{{$number or "NUMBER"}}" aria-expanded="false" aria-controls="collapseExample{{$number or "NUMBER"}}">
        New wish
    </a>
    <a class="delete-new-wish">
        <span class="glyphicon glyphicon-trash pull-right"></span>
    </a>
    <div class="collapse in" id="collapseExample{{$number or "NUMBER"}}">
        <hr>
        {{--<div class="well">--}}
            <div class="form-group">
                <label for="wishes-name{{$number or "NUMBER"}}">Name:</label>
                <input type="text" class="form-control" name="wishes[{{$number or "NUMBER"}}][name]" id="wishes-name{{$number or "NUMBER"}}" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="wishes-description{{$number or "NUMBER"}}">Description:</label>
                <textarea class="form-control" name="wishes[{{$number or "NUMBER"}}][description]" id="wishes-description{{$number or "NUMBER"}}"
                          placeholder="Description"></textarea>
            </div>
            <div class="form-group">
                <label for="wishes-price{{$number or "NUMBER"}}">Price:</label>
                <input type="number" min="0.00" step="0.01" value="0.00" class="form-control" name="wishes[{{$number or "NUMBER"}}][price]" id="wishes-price{{$number or "NUMBER"}}" placeholder="Price">
            </div>
            <input type="hidden" name="wishes[{{$number or "NUMBER"}}][position]" value="{{$number or "NUMBER"}}">
            <input type="hidden" name="wishes[{{$number or "NUMBER"}}][is_checked]" value="0">
        {{--</div>--}}
    </div>
{{--<hr>--}}
</li>