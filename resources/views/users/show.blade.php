@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="row">
            <h1>Profile</h1>
            <div class="col-md-2">
                Username:
            </div>
            <div class="col-md-10">
                {{ $user->name }}
            </div>
            <div class="col-md-2">
                Email:
            </div>
            <div class="col-md-10">
                {{ $user->email }}
            </div>
            <div class="row">
                @if(!Auth::guest() && $user->id == Auth::user()->id)
                    <div class="pull-right">
                        <a class="btn btn-primary btn-sm" href="/wishlists/create">Add new wishlist</a>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">Wishlists:</div>
                    <div class="panel-body">
                        <ul>
                            @foreach($user->wishlists as $wishlist)
                                @if(Auth::guest() || $user->id != Auth::user()->id)
                                    @if($wishlist->is_private == false)
                                        <li class="wishlists-item" data-url="/wishlists/{{ $wishlist->id }}">
                                            <div class="wishlist-title">{{ $wishlist->name }}
                                            </div>
                                            <div class="wishes-list">
                                                <ul>
                                                    @foreach($wishlist->wishesOrderedByPosition() as $wish)
                                                        <li class="wishes-item">{{ $wish->name }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>
                                    @endif
                                @else
                                    @include('wishlists.users-wishlist')
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
