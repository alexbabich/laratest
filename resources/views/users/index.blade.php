@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Users</div>
            <div class="panel-body">
                <table class="table table-responsive table-hover">
                    @foreach($users as $user)
                        <tr>
                            <td><a href="users/{{$user->id}}">{{$user->name}}</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@stop
