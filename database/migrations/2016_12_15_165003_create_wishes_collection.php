<?php

//use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;

class CreateWishesCollection extends Migration
{
    public function up()
    {
        Schema::connection($this->connection)
            ->create('wishes', function (Blueprint $collection) {
                $collection->increments('id');
                $collection->text('name');
                $collection->longText('description');
                $collection->integer('position');
                $collection->index('wishlist_id');
                $collection->float('price');
                $collection->boolean('is_checked');
//            $collection->timestamps();
            });
    }

    public function down()
    {
        Schema::connection($this->connection)
            ->create('wishes', function (Blueprint $collection)
            {
                $collection->drop();
            });
    }
}
