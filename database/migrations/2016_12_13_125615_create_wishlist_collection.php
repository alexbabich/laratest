<?php

//use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;

class CreateWishlistCollection extends Migration
{
    protected $connection = 'mongodb';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)
        ->create('wishlists', function (Blueprint $collection) {
            $collection->increments('id');
            $collection->text('name');
            $collection->longText('description');
            $collection->boolean('is_private');
            $collection->index('user_id');
//            $collection->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)
            ->create('wishlists', function (Blueprint $collection)
            {
                $collection->drop();
            });
    }
}
