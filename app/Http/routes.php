<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return redirect('users');
    });

    Route::get('users/', 'UsersController@index');
    Route::get('users/{user}', 'UsersController@show');

    Route::get('wishlists', 'WishlistsController@index')->middleware('auth');
    Route::get('wishlists/create', 'WishlistsController@create')->middleware('auth');
    Route::get('wishlists/{wishlist}', 'WishlistsController@show');
    Route::get('wishlists/{wishlist}/edit', 'WishlistsController@edit')->middleware('auth');
    Route::post('wishlists', 'WishlistsController@store')->middleware('auth');
    Route::put('wishlists/{wishlist}', 'WishlistsController@update')->middleware('auth');
    Route::patch('wishlists/{wishlist}', 'WishlistsController@update')->middleware('auth');
    Route::delete('wishlists/{wishlist}', 'WishlistsController@destroy')->middleware('auth');
    Route::patch('wishlists/{wishlist}/order-wishes', 'WishlistsController@orderWishes')->middleware('auth');

//Route::get('wishes', 'WishesController@index');
//Route::get('wishes/create', 'WishesController@create');
//Route::get('wishes/{wish}', 'WishesController@show');
//Route::get('wishes/{wish}/edit', 'WishesController@edit');
//Route::post('wishes', 'WishesController@store');
//Route::put('wishes/{wish}', 'WishesController@update');
//Route::patch('wishes/{wish}', 'WishesController@update');
    Route::patch('wishes/{wish}/check', 'WishesController@check')->middleware('auth');
    Route::delete('wishes/{wish}', 'WishesController@destroy')->middleware('auth');

    Route::auth();

    Route::get('/home', 'UsersController@index');

});
