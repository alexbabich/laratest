<?php

namespace App\Http\Controllers;

use App\Wish;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class WishesController extends Controller
{
    public function destroy(Wish $wish)
    {
        $wish->delete();
        return ['status' => 'success'];
    }

    public function check(Request $request, Wish $wish)
    {
        $isChecked = $request->get('is_checked');
        if (null === $isChecked) {
            return ['status' => 'failure'];
        }
        $wish->is_checked = $isChecked;
        $wish->save();

        return [
            'status' => 'success',
            'checked' => $wish->is_checked
        ];
    }
}
