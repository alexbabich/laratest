<?php

namespace App\Http\Controllers;

use App\Wish;
use App\Wishlist;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class WishlistsController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
        $wishlists = $user->wishlists;

        return view('wishlists.index', compact('wishlists'));
    }

    public function show(Wishlist $wishlist)
    {
        $user = $this->getAuthUserOrFalse();
        if ($user
            && $user->id !== $wishlist->user->id
            && !empty($wishlist->is_private)
        ) {
            return redirect('wishlists');
        }
        $wishes = $wishlist->wishesOrderedByPosition();

        return view('wishlists.show', ['wishlist' => $wishlist, 'wishes' => $wishes]);
    }

    public function edit(Wishlist $wishlist)
    {
        if (Auth::user()->id !== $wishlist->user->id) {
            return redirect('wishlists');
        }
        $wishes = $wishlist->wishesOrderedByPosition();

        return view('wishlists.edit', ['wishlist' => $wishlist, 'wishes' => $wishes]);
    }

    public function create()
    {
        return view('wishlists.create');
    }

    public function store(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $wishlist = null;
        if ($wishlistArray = $request->get('wishlist')) {
            $wishlist = new Wishlist($wishlistArray);
        }
        $user->addWishlist($wishlist);
        if ($wishesArray = $request->get('wishes')) {
            foreach ($wishesArray as $wishData) {
                $wish = new Wish($wishData);
                $wishlist->addWish($wish);
            }
        }

        return redirect(sprintf('/users/%s', $user->id));
    }

    public function update(Request $request, Wishlist $wishlist)
    {
        if ($wishlistArray = $request->get('wishlist')) {
            $wishlist->update($wishlistArray);
        }
        if ($wishesArray = $request->get('wishes')) {
            foreach ($wishesArray as $wishData) {
                if (isset($wishData['id'])) {
                    $wish = Wish::find($wishData['id']);
                    $wish->update($wishData);
                } else {
                    $wish = new Wish($wishData);
                    $wishlist->addWish($wish);
                }
            }
        }

        return view('wishlists.show', compact('wishlist'));
    }

    public function orderWishes(Request $request, Wishlist $wishlist)
    {
        $wishesIdArray = $request->get('item');
        if ($wishesIdArray && count($wishesIdArray)) {
            foreach ($wishesIdArray as $index => $wishId) {
                $wish = Wish::find($wishId);
                if (empty($wish)) {
                    continue;
                }
                $wish->update(['position' => $index]);
            }
        }

        return [];
    }

    public function destroy(Wishlist $wishlist)
    {
        $wishlist->delete();

        return ['status' => 'success'];
    }

    /**
     * @return bool| User
     */
    private function getAuthUserOrFalse()
    {
        return Auth::user() ?? false;
    }
}
