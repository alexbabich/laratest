<?php

namespace App;

use App\Security\MongodbAuthenticatable;

class User extends MongodbAuthenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $collection = 'users';

    public function wishlists()
    {
        return $this->hasMany(Wishlist::class);
    }

    public function addWishlist(Wishlist $wishlist)
    {
        return $this->wishlists()->save($wishlist);
    }
}
