<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Wishlist extends Model
{
    protected $connection = 'mongodb';
    protected $fillable = ['name', 'description', 'is_private'];

    protected $collection = 'wishlists';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wishes()
    {
        return $this->hasMany(Wish::class);
    }

    public function wishesOrderedByPosition()
    {
        return $this->hasMany(Wish::class)->orderBy('position', 'asc')->get();
    }

    public function addWish(Wish $wish)
    {
        return $this->wishes()->save($wish);
    }
}
