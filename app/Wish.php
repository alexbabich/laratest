<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Wish extends Model
{
    protected $connection = 'mongodb';
    protected $fillable = ['name', 'description', 'position', 'price', 'is_checked'];

    protected $collection = 'wishes';

    public function wishlist()
    {
        return $this->belongsTo(Wishlist::class);
    }
}
